import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate,useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match. 
		if((email !== "" && password1 !== "" && password2 !== "" && email !== "" && password1 !== "" && password2 !== "") && (password2 === password1) && (mobileNo.toString().length > 10)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[firstName, lastName, mobileNo, email, password1, password2]);

	function registerUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkemail`,{
			
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data == true){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			}
			else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data == true){

						setFirstName("");
						setLastName("");
						setMobileNo("");
						setEmail("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						});

	                  	navigate("/courses");
					}
					else{

						Swal.fire({
							title: "Some error has occured",
							icon: "error",
							text: "Please try again!"
						});
					}
				})
			}
		})


	};
	
	return (
		(user.id !== null) ? 
		<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name "
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name "
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile No</Form.Label>
				<Form.Control
					type="Number"
					placeholder="Enter mobile number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ 
				isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>

			}

		</Form>
	)
}